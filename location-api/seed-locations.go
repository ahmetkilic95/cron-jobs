package cron

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/go-co-op/gocron"
)

type CouchbaseDb interface {
	SeedData()
}

func SeedCountryData(db CouchbaseDb) {
	fmt.Println("seed data has been called")
	min := 10
	max := 59
	date := fmt.Sprintf("23:%d;%d:00", rand.Intn(max-min)+min, rand.Intn(max-min)+min)
	fmt.Println(date)
	job := gocron.NewScheduler(time.UTC)
	job.Every(1).Month().At(date).Do(func() {
		db.SeedData()
	})

	job.StartAsync()
}
